﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OHMValueCalcuator_Test
{

    /// <summary>
    /// Checking Test for Random Values : Failed Case
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test1checkForValuesGivenInAllBands()
        {
            OhmValueCalculator.Classes.OhmValueCalculatorHelperClass calculator = new OhmValueCalculator.Classes.OhmValueCalculatorHelperClass();
            var res = calculator.CalculateOhmValue("Orange", "Brown", "Black", "Red");
            Assert.AreEqual(res, 54);

        }
        /// <summary>
        ///  Checking Test for Random Values : Pass Case
        /// </summary>
        [TestMethod]
        public void Test2checkForValuesGivenInAllBands()
        {
            OhmValueCalculator.Classes.OhmValueCalculatorHelperClass calculator = new OhmValueCalculator.Classes.OhmValueCalculatorHelperClass();
            var res = calculator.CalculateOhmValue("Green", "Yellow", "Gold", "Red");
            Assert.AreEqual(res, Convert.ToInt64(6));

        }
        /// <summary>
        /// Test case to check result when tolerance band value is None
        /// </summary>    
        [TestMethod]
        public void Test3checkForToleranceBandNone()
        {
            OhmValueCalculator.Classes.OhmValueCalculatorHelperClass calculator = new OhmValueCalculator.Classes.OhmValueCalculatorHelperClass();
            var res = calculator.CalculateOhmValue("Green", "Yellow", "Gold", "None");
            Assert.AreEqual(res, 79);

        }
        /// <summary>
        /// Test case if result is null
        /// </summary>   
      
        [TestMethod]
        public void Test4checkForIsNullTest()
        {
            OhmValueCalculator.Classes.OhmValueCalculatorHelperClass calculator = new OhmValueCalculator.Classes.OhmValueCalculatorHelperClass();
            var res = calculator.CalculateOhmValue("Red", "Yellow", "Green", "Blue");
            Assert.IsNotNull(res);

        }
    }
}
