ORIGINAL REQUIREMENT:

Electronic color code

The electronic color code (http://en.wikipedia.org/wiki/Electronic_color_code) is used to indicate the
values or ratings of electronic components, very commonly for resistors. Write a class that implements
the following interface. Feel free to include any supporting types as necessary.
public interface IOhmValueCalculator
{
   /// &lt;summary&gt;
   /// Calculates the Ohm value of a resistor based on the band colors.
   /// &lt;/summary&gt;
   /// &lt;param name=&quot;bandAColor&quot;&gt;The color of the first figure of component value band.&lt;/param&gt;
   /// &lt;param name=&quot;bandBColor&quot;&gt;The color of the second significant figure band.&lt;/param&gt;
   /// &lt;param name=&quot;bandCColor&quot;&gt;The color of the decimal multiplier band.&lt;/param&gt;
   /// &lt;param name=&quot;bandDColor&quot;&gt;The color of the tolerance value band.&lt;/param&gt;
   int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor);
}

LIVE SITE INFO, INSTRUCTIONS AND MODIFICATIONS:

The Calculator deployed on Azure at: http://ohmvaluecalculator.azurewebsites.net/

The Method Return Type is modified from int to long:(is modified to long to accommodate out of range values for int method will look like) 
FROM: int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor); 
TO: long CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor);




To Run the Application Locally: by downloading the zip file and opening the csproj file in Visual Studio 2017 
Test Can be Run in Visual Studio 2017 from the Test Explorer(From Menu Optons Test >> Windows >> Test Explorer) and Then Running 'Run All Tests'


